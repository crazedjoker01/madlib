#include <conio.h>
#include <iostream>
#include <string>
#include <array>
#include <fstream>

using namespace std;

int main()
{
	string path = "C:\\Users\\500180423\\Desktop\\madlib.txt";
	ofstream saveFile(path);
	const int ARRAY_SIZE = 12;
	string madlib[ARRAY_SIZE] = { "adjective", "sport", "city", "person", "action verb", "vehicle", "place", "noun", "adjective", "food", "liquid", "adjective" };
	for (int i = 0; i < ARRAY_SIZE; i++) 
	{
		cout << "\nEnter a " << madlib[i] << ": ";
		cin >> madlib[i]; 
	}

	cout << "One day my " << madlib[0] << " friend and I decided to go to the " << madlib[1] << " game in " << madlib[2] << " We really wanted to see " << madlib[3] << " play.\n"
		<< " So we" << madlib[4] << " in the " << madlib[5] << " and headed down to " << madlib[6] << " and bought some " << madlib[7] << "\nWe watched the game and it was " << madlib[8]
		<< ". We ate some " << madlib[9] << " and drank some " << madlib[10] << ". \nWe had a " << madlib[11] << " time, and can't wait to go again. ";

	char save;
	cout << "Do you want to save this madlib as a text file?(y/n)";
	cin >> save;
	if (save == 'y')
	{
		saveFile << "One day my " << madlib[0] << " friend and I decided to go to the " << madlib[1] << " game in " << madlib[2] << " We really wanted to see " << madlib[3] << " play.\n"
			<< " So we" << madlib[4] << " in the " << madlib[5] << " and headed down to " << madlib[6] << " and bought some " << madlib[7] << "\nWe watched the game and it was " << madlib[8]
			<< ". We ate some " << madlib[9] << " and drank some " << madlib[10] << ". \nWe had a " << madlib[11] << " time, and can't wait to go again. ";
		saveFile.close();
	}

	_getch();
	return 0;
}